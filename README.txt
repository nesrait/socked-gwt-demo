== Install maven
http://askubuntu.com/a/94341

== Install nodejs
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs

== Install other nodejs dependencies
# https://github.com/sockjs/sockjs-node
npm install socksjs
# http://expressjs.com/guide.html
npm install express

== Build project
mvn clean compile gwt:compile jetty:run -Djetty.port=9000

This will build and then serve the gwt application with a jetty server listening on port 9000.

== Startup socked-node server

Instructions here:
https://github.com/davide/socked-node

== Check if it's working!

Open http://localhost:9000/ on your browser and check the console output.

You should see something like this:
  SockedExample - #conn-s1: subscribing mouselive
  ["Socked: connection s1 established!"]
  SockedExample - #conn-s1: connected to mouselive
  <mouselive> hello mouselive! #conn-s1 here!
  SockedExample - #conn-s1: disconnected from mouselive

